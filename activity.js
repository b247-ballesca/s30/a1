//1

db.fruits.aggregate([
  {
      $match: {
        onSale: {
          $eq: true
        }
      }
    },
    {
      $count: "fruitsonsale"
    }
        
]);




// 2


db.fruits.aggregate([
    {
      $match: { $or: [ { stock: { $gt: 20} }, { stock: 20 } ] } 
    },
    {
      $count: "enoughStock"
    }
        
]);

// OR PROBABLY THE EASIEST WAY :D
db.fruits.aggregate([
  {
      $match: {
        stock: { 
          $gt: 19
        }
      }
    },
    {
      $count: "enoughStock"
    }
        
]);

// 3

db.fruits.aggregate([
    { $match: { onSale: true} },{
       $group:
         {
           _id: "$supplier_id",
           avgAmount: { $avg: "$price" }
         }
     }
]);

// 4

db.fruits.aggregate([
 { $match: { onSale: true} },{
       $group:
         {
           _id: "$supplier_id",
           max_price: { $max: "$price" }
         }
     }
]);

// 5

db.fruits.aggregate([
 { $match: { onSale: true} },{
       $group:
         {
           _id: "$supplier_id",
           max_price: { $min: "$price" }
         }
     }
]);

